package com.ecommerce.order.detail.controllers;

import com.ecommerce.order.detail.model.dto.OrderDto;
import com.ecommerce.order.detail.model.dto.ProductOrderByUserDto;
import com.ecommerce.order.detail.services.impl.OrderServiceImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: OrderController <br/>
 * .
 *
 * @author Carlos <br/>
 */
@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {
  private final OrderServiceImpl orderService;

  @GetMapping("/products/{userId}")
  public Mono<ProductOrderByUserDto> findProductsByUserId(@PathVariable final String userId) {
    return orderService.findProductsOrderByUserId(userId);
  }

  @PostMapping
  public Mono<OrderDto> saveOrder(@RequestBody final OrderDto orderDto) {
    return orderService.saveOrders(orderDto);
  }

}
