package com.ecommerce.order.detail.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: OrderDetailsDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OrderDetailsDto {
  private Long productId;
}
