package com.ecommerce.order.detail.model.dto;

import com.ecommerce.order.detail.webclient.response.ProductDto;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: ProductOrderByUserDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductOrderByUserDto {
  private String userId;
  private List<ProductDto> products;
}
