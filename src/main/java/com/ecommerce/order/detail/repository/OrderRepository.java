package com.ecommerce.order.detail.repository;

import com.ecommerce.order.detail.model.entity.Order;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <b>Class</b>: OrderRepository <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
  List<Order> findByUserId(String userId);
}
