package com.ecommerce.order.detail.mappers;

import com.ecommerce.order.detail.model.dto.OrderDetailsDto;
import com.ecommerce.order.detail.model.dto.OrderDto;
import com.ecommerce.order.detail.model.entity.Order;
import com.ecommerce.order.detail.model.entity.OrderDetails;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: OrderMapper <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OrderMapper {

  /**
   * orderDtoToOrder .
   *
   * @param orderDto .
   * @return
   */
  public static Order orderDtoToOrder(final OrderDto orderDto) {
    final Order order = Order.builder()
        .userId(orderDto.getUserId())
        .total(orderDto.getTotal())
        .build();

    order.setDetails(orderDetailsDtoListToOrderDetailsList(orderDto.getDetails(), order));
    return order;
  }

  /**
   * orderToOrderDto .
   *
   * @param order .
   * @return
   */
  public static OrderDto orderToOrderDto(final Order order) {
    return OrderDto.builder()
        .userId(order.getUserId())
        .total(order.getTotal())
        .details(order.getDetails().stream()
            .map(orderDetails -> OrderDetailsDto.builder()
                .productId(orderDetails.getProductId())
                .build())
            .collect(Collectors.toList()))
        .build();
  }

  /**
   * orderListToProductIdList .
   *
   * @param orders .
   * @return
   */
  public static Mono<List<Long>> orderListToProductIdList(final List<Order> orders) {
    return Flux.fromIterable(orders)
        .map(Order::getDetails)
        .flatMap(o -> Flux.fromIterable(o)
            .map(OrderDetails::getProductId)
        ).collect(Collectors.toList());
  }

  /**
   * orderDetailsDtoListToOrderDetailsList .
   *
   * @param orderDetails .
   * @param order        .
   * @return
   */
  private static List<OrderDetails> orderDetailsDtoListToOrderDetailsList(final List<OrderDetailsDto> orderDetails,
      final Order order) {
    return orderDetails.stream()
        .map(orderDetailsDto -> OrderDetails.builder()
            .productId(orderDetailsDto.getProductId())
            .order(order)
            .build())
        .collect(Collectors.toList());
  }

}
