package com.ecommerce.order.detail.services;

import com.ecommerce.order.detail.model.dto.OrderDto;
import com.ecommerce.order.detail.model.dto.ProductOrderByUserDto;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: OrderService <br/>
 * .
 *
 * @author Carlos <br/>
 */
public interface OrderService {
  Mono<OrderDto> saveOrders(OrderDto orderDto);

  Mono<ProductOrderByUserDto> findProductsOrderByUserId(String userId);
}
