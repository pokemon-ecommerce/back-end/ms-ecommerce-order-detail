package com.ecommerce.order.detail.services.impl;

import com.ecommerce.order.detail.mappers.OrderMapper;
import com.ecommerce.order.detail.model.dto.OrderDto;
import com.ecommerce.order.detail.model.dto.ProductOrderByUserDto;
import com.ecommerce.order.detail.repository.OrderRepository;
import com.ecommerce.order.detail.services.OrderService;
import com.ecommerce.order.detail.webclient.ProductApi;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: OrderServiceImpl <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final ProductApi productApi;

  @Override
  public Mono<OrderDto> saveOrders(final OrderDto orderDto) {
    return Mono.fromCallable(() -> OrderMapper.orderDtoToOrder(orderDto))
        .map(orderRepository::save)
        .map(OrderMapper::orderToOrderDto);
  }

  @Override
  public Mono<ProductOrderByUserDto> findProductsOrderByUserId(final String userId) {
    return Mono.fromCallable(() -> orderRepository.findByUserId(userId))
        .map(OrderMapper::orderListToProductIdList)
        .flatMap(monoIdList -> monoIdList.map(Flux::fromIterable))
        .flatMap(fluxId -> fluxId
            .flatMap(productApi::findProductById)
            .collect(Collectors.toList()))
        .map(products -> ProductOrderByUserDto.builder()
            .userId(userId)
            .products(products)
            .build());
  }

}
