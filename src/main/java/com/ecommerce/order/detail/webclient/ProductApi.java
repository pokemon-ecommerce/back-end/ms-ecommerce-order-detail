package com.ecommerce.order.detail.webclient;

import com.ecommerce.order.detail.webclient.response.ProductDto;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: ProductApi <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Repository
@RequiredArgsConstructor
public class ProductApi {

  private final WebClient productApiClient;

  /**
   * findProductById .
   *
   * @param productId .
   * @return
   */
  public Mono<ProductDto> findProductById(final Long productId) {
    return productApiClient.get()
        .uri("/products/{id}", productId)
        .retrieve()
        .bodyToMono(ProductDto.class).log();
  }
}
