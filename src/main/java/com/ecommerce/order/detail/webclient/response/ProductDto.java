package com.ecommerce.order.detail.webclient.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Class</b>: ProductDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductDto {
  private Long id;
  private String name;
  private String description;
  private String image;
}
