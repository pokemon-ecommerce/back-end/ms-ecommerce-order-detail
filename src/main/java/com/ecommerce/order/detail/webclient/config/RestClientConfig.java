package com.ecommerce.order.detail.webclient.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: RestClientConfig <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Configuration
@Slf4j
public class RestClientConfig {

  @Value("${api.product.baseUrl}")
  private String productApiBaseUrl;

  /**
   * productApiClient .
   *
   * @return
   */
  @Bean
  public WebClient productApiClient() {
    return WebClient.builder()
        .baseUrl(productApiBaseUrl)
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .filter(logRequest())
        .build();
  }

  private static ExchangeFilterFunction logRequest() {
    return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
      log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
      clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
      return Mono.just(clientRequest);
    });
  }

}
